package com.tw;

public class StringCalculator {
    public int add(String string) {
        if (string.isEmpty()) return 0;
        int sum = 0;

        String[] splitStr = string.split("[,\\n;]");

        for(String str: splitStr){
            try {
                sum += Integer.parseInt(str);
            } catch (NumberFormatException e) {
                System.out.println("Exception: " + e);
            }
        }
        return sum;
    }
}
